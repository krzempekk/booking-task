# Talixo Front End Task
## Uruchamianie
Uruchamianie projektu przebiega standardowo jak w przypadku każdej aplikacji stworzonej za pomocą create-react-app. Najpierw instalujemy potrzebne biblioteki za pomocą npm install, a potem odpalamy aplikację za pomocą npm start. Używałem Node.JS w wersji 8.12.0 oraz npm w wersji 6.4.1.
## Komentarz
Z racji ograniczonego czasu (nałożyło mi się na to wiele obowiązków związanych z uczelnią) projekt jest niekompletny. Implementuję pierwszy krok procesu bookingu. Brakuje optymalizacji pod kątem RWD, pól do sprecyzowania lotu pojawiających się po wybraniu lotniska jako miejsce źródłowe, przy wyborze daty nie ma możliwości szybkiego wybrania następnego lub poprzedniego dnia.  
Struktura projektu umożliwia jednak rozwijanie go. Objętość renderowanej templatki w kodzie jest ograniczona przez zapisywanie informacji o polach w stanie komponentu, co umożliwia również łatwe dodawanie kolejnych pól. Nie byłoby problemu z zaimplementowaniem kolejnych kroków oraz połączenia formularza z backendem. Stworzyłem również podstawy pod frontendową walidację.  
