import React, { Component } from 'react';

import BookingForm from "../BookingForm/BookingForm";

class App extends Component {
  render() {
    return (
      <div className="App">
        <BookingForm/>
      </div>
    );
  }
}

export default App;
