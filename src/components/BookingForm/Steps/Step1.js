import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip';
import DatePicker from "react-datepicker";

import getDropdownItems from "../../../external/getDropdownItems";

import "react-datepicker/dist/react-datepicker.css";

import icon_help from "../../../img/icon_help.png";
import icon_passengers from "../../../img/icon_passengers.png";
import icon_luggage from "../../../img/icon_luggage.png";
import icon_equipment from "../../../img/icon_equipment.png";
import icon_animals from "../../../img/icon_animals.png";
import icon_children from "../../../img/icon_children.png";

class Step1 extends Component {

    constructor(props) {
        super(props);

        // state represents detailed info about fields, including elements of dropdown
        this.state = {

            fieldList: {
                placeFrom: {
                    label: "Pick up",
                    placeholder: "e.g. Torstrasse 124, Berlin",
                    tooltip: "Please provide a street address, airport name or hotel name.",
                    textInput: true,
                    dropdownItemsFetch: "places",
                    dropdown: {
                        elements: [],
                        hidden: true,
                        loading: false,
                    }
                },
                placeTo: {
                    label: "Destination",
                    placeholder: "e.g. Tegel Airport",
                    textInput: true,
                    dropdownItemsFetch: "places",
                    tooltip: "Please provide a street address, airport name or hotel name",
                    dropdown: {
                        elements: [],
                        hidden: true,
                        loading: false,
                    }
                },
                date: {
                    label: "Date",
                    specialField: "date",
                    maxInline: 2,
                },
                time: {
                    label: "At",
                    textInput: true,
                    maxInline: 2,
                    dropdown: {
                        elements: ["15:00", "15:15", "15:30", "15:45"],
                        hidden: true,
                    }
                },
                voucherCode: {
                    label: "Voucher code (optional)",
                    textInput: true,
                },
                passengers: {
                    icon: icon_passengers,
                    textInput: false,
                    maxInline: 3,
                    tooltip: "Passengers",
                    dropdown: {
                        elements: [1, 2, 3, 4, 5],
                        hidden: true,
                    }
                },
                luggage: {
                    icon: icon_luggage,
                    tooltip: "max. 20kg each. 1 piece of hand luggage is included per passenger",
                    maxInline: 3,
                    dropdown: {
                        elements: [1, 2, 3, 4, 5],
                        hidden: true,
                    }
                },
                equipment: {
                    icon: icon_equipment,
                    tooltip: "Golf equipment, Skis, Snowboard",
                    maxInline: 3,
                    dropdown: {
                        elements: [0, 1, 2, 3, 4],
                        hidden: true,
                    }
                },
                animals: {
                    icon: icon_animals,
                    tooltip: "Small animals",
                    maxInline: 3,
                    dropdown: {
                        elements: [0, 1, 2, 3, 4],
                        hidden: true,
                    }
                },
                children: {
                    icon: icon_children,
                    tooltip: "Children seats",
                    maxInline: 3,
                    dropdown: {
                        elements: [0, 1, 2],
                        hidden: true,
                    }
                }
            },
            optionsHidden: true
        }
    }

    fetchDropdownItems(field) {
        const newState = this.state;
        newState.fieldList[field].dropdown.loading = true;
        this.setState(newState);
        getDropdownItems(this.props[field], newState.fieldList[field].dropdownItemsFetch)
            .then((elements) => {
                const newState = this.state;
                newState.fieldList[field].dropdown["elements"] = elements;
                newState.fieldList[field].dropdown["hidden"] = false;
                newState.fieldList[field].dropdown["loading"] = false;
                this.setState(newState);
            });
    }

    componentDidUpdate(prevProps, prevState, snapshot) { // fetching places from (mocked) backend based on user input
        const { fieldList } = this.state;
        Object.keys(fieldList)
            .filter((field) => !!fieldList[field].dropdownItemsFetch)
            .forEach((field) => {
                if(prevProps[field] !== this.props[field] && fieldList[field].dropdown.elements.indexOf(this.props[field]) === -1) {
                    this.fetchDropdownItems(field);
                }
            })
    }

    switchDropdownVisibility(field) {
        const newState = this.state;
        newState.fieldList[field].dropdown.hidden = !newState.fieldList[field].dropdown.hidden;
        this.setState(newState);
    }

    render() {
        const { handleChange, handleSubmit, activateValidation, errorList } = this.props;
        const { fieldList } = this.state;

        return (
            <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                <ReactTooltip effect="solid" place="right" event="click"/>
                {
                    Object.keys(fieldList).map((fieldName, index) => {
                            const field = fieldList[fieldName];
                            if(field.specialField === "date") {
                                return (
                                    <div className={"Form-Field " + (field.maxInline ? "Inline" + field.maxInline : "")} key={index}>
                                        <label>{field.label}</label>
                                        <DatePicker
                                            selected={this.props[fieldName]}
                                            onChange={(date) => handleChange({name: "date", value: date})}
                                        />
                                    </div>
                                );
                            }
                            return (
                                <div className={"Form-Field " + (field.maxInline ? "Inline" + field.maxInline : "")} key={index}>
                                    {
                                        field.icon
                                            ?
                                                <label className="iconLabel" htmlFor={fieldName}><img src={field.icon} alt={field.tooltip} data-tip={field.tooltip}/></label>
                                            :
                                                <label htmlFor={fieldName}>{field.label}</label>

                                    }
                                    <input type="text" name={fieldName} value={this.props[fieldName]}
                                           placeholder={field.placeholder} onChange={field.textInput ? handleChange : undefined} readOnly={!field.textInput} onBlur={activateValidation} required/>

                                    {
                                        field.dropdown &&
                                        <span
                                            className={`Dropdown ${field.dropdownItemsFetch && field.dropdown.loading ? "Loader" : (field.dropdown.hidden ? "Expand" : "Hide")}`}
                                            onClick={() => this.switchDropdownVisibility(fieldName)}> </span>
                                    }

                                    {
                                        field.tooltip && !field.maxInline &&
                                                <span className="Hint" data-tip={field.tooltip}><img src={icon_help} alt="Hint"/></span>
                                    }

                                    {
                                        errorList[fieldName] && errorList[fieldName].length > 0 &&
                                            <div className="Error">{errorList[fieldName][0]}</div>
                                    }

                                    {
                                        field.dropdown && !field.dropdown.hidden &&
                                        <ul>
                                            {
                                                field.dropdown.elements.map((item, index) => (
                                                    <li key={index} data-for={fieldName} onClick={(e) => {
                                                        handleChange(e);
                                                        this.switchDropdownVisibility(fieldName)
                                                    }}>{item}</li>
                                                ))
                                            }
                                        </ul>
                                    }
                                </div>

                            )
                        }
                    )
                }
                <input type="submit" value="Start booking"/>
            </form>
        )
    }
}

export default Step1;