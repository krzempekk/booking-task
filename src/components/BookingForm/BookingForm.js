import React, { Component } from 'react';

import Step1 from "./Steps/Step1";

import "./BookingForm.scss";

class BookingForm extends Component {

    constructor(props) {
        super(props);

        // state represents values, array of errors, whether the validation is active and types of validation of fields
        this.state = {
            stepsFieldLists: [
                {
                    placeFrom: {
                        value: '',
                        errors: [],
                        validations: ["notEmpty"]
                    },
                    placeTo: {
                        value: '',
                        errors: [],
                        validations: ["notEmpty"]
                    },
                    date: {
                        value: new Date(),
                    },
                    time: {
                        value: "",
                        validations: ["timeFormat"]
                    },
                    voucherCode: {
                        value: '',
                    },
                    passengers: {
                        value: 2,
                    },
                    luggage: {
                        value: 2,
                    },
                    equipment: {
                        value: 0,
                    },
                    animals: {
                        value: 0,
                    },
                    children: {
                        value: 0,
                    }
                }
            ],
            currentStep: 1
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.activateValidation = this.activateValidation.bind(this);
    }


    activateValidation(event) { // activation for field is triggered when user unfocus field (and after that moment after every change)
        const { name } = event.target, newState = this.state;
        const currentField = newState.stepsFieldLists[this.state.currentStep - 1][name];
        if(!currentField.validationActive) {
            currentField.validationActive = true;
            this.validate(name, newState);
            this.setState(newState);
        }
    }

    validate(name, state, force = false) { // trigger validations for specified field and save errors (if any) to state
        const validatedField = state.stepsFieldLists[state.currentStep - 1][name];
        if((force || validatedField.validationActive) && validatedField.validations) { // validations are triggered if validation for field is active or if we force it by passing force argument
            validatedField.errors = [];
            validatedField.validations.forEach((validation) => {
               if(validation === "notEmpty" && validatedField.value === "") {
                   validatedField.errors.push("Field must not be empty");
               }
               else if(validation === "timeFormat" && !/^\d\d:\d\d$/.test(validatedField.value)) {
                   validatedField.errors.push("Invalid time format, it should be HH:MM");
               }
            });
        }
    }

    handleChange(event) { // function can be invoked from various types of change events, we have to adjust our behaviour accordingly
        let name, value;
        if(event.target) {
            if(event.target.name) {
                name = event.target.name; value = event.target.value;
            }
            else {
                name = event.target.dataset.for; value = event.target.textContent;
            }
        }
        else {
            name = event.name; value = event.value;
        }
        const newState = this.state;
        if(name === "time") {
            newState.stepsFieldLists[0].date.value.setHours(...value.split(":"), 0);
        }
        newState.stepsFieldLists[this.state.currentStep - 1][name].value = value;
        this.validate(name, newState);
        this.setState(newState);
    }

    handleSubmit(event) {
        event.preventDefault();
        const newState = this.state;
        let errors = false;
        Object.keys(newState.stepsFieldLists[newState.currentStep - 1]).forEach((key) => {
            if(newState.stepsFieldLists[0][key].errors) {
                this.validate(key, newState, true);
                errors = errors || newState.stepsFieldLists[0][key].errors.length;
            }
        })
        if(!errors) {
            //TODO: send form to backend for further validation. insert errors to state, if any. if no errors from backend we can go to the next step
        }
        this.setState(newState);
    }

    render() {
        const step1Fields = {}, step1Errors = {};
        Object.keys(this.state.stepsFieldLists[0]).forEach((key) => {
            step1Fields[key] = this.state.stepsFieldLists[0][key].value;
            step1Errors[key] = this.state.stepsFieldLists[0][key].errors;
        })

        return (
            <div className="BookingForm">
                <Step1 {...step1Fields} errorList={step1Errors} activateValidation={this.activateValidation} handleChange={this.handleChange} handleSubmit={this.handleSubmit}/>
            </div>
        )
    }
}

export default BookingForm;
